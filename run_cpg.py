# SPDX-FileCopyrightText: Copyright (c) 2022 Guillaume Bellegarda. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Copyright (c) 2022 EPFL, Guillaume Bellegarda

""" Run CPG """
import numpy as np
from matplotlib import pyplot as plt

from env.hopf_network import HopfNetwork
from env.quadruped_gym_env import QuadrupedGymEnv

# adapt as needed for your system

from sys import platform
# if platform =="darwin":
#   matplotlib.use("Qt5Agg")
# else:
#   matplotlib.use('TkAgg')

ADD_CARTESIAN_PD = True  # default: false
TIME_STEP = 0.001
foot_y = 0.0838  # this is the hip length
sideSign = np.array([-1, 1, -1, 1])  # get correct hip sign (body right is negative)

env = QuadrupedGymEnv(render=True,  # visualize
                      on_rack=False,  # useful for debugging!
                      isRLGymInterface=False,  # not using RL
                      time_step=TIME_STEP,
                      action_repeat=1,
                      motor_control_mode="TORQUE",
                      add_noise=False,  # start in ideal conditions
                      #record_video=True
                      )

# initialize Hopf Network, supply gait
cpg = HopfNetwork(time_step=TIME_STEP)
SIM_TIME = 0.25
TEST_STEPS = int(SIM_TIME / (TIME_STEP))  # default: 10 seconds
t = np.arange(TEST_STEPS) * TIME_STEP

# [] initialize data structures to save CPG and robot states
# 1. A plot of the CPG states (r, θ, r ̇, θ ̇) for a trot gait (plots for other gaits are encouraged, but not required).
# We suggest making subplots for each leg, and make sure these are at a scale where the states are clearly visible (for example 2 gait cycles).
# 2. A plot comparing the desired foot position vs. actual foot position with/without joint PD and Cartesian PD (for one leg is fine).
# What gains do you use, and how does this affect tracking performance?
# 3. A plot comparing the desired joint angles vs. actual joint angles with/without joint PD and Cartesian PD (for one leg is fine).
CPG_STATES = np.zeros((4, 4, TEST_STEPS))  # LEG/ r theta r. theta. /time

DESIRED_FOOT_POSITIONS = np.zeros((3, TEST_STEPS))  # ONLY LEG1
ACTUAL_FOOT_POSITIONS = np.zeros((3, TEST_STEPS))  # ONLY LEG1
DESIRED_JOINT_ANGLES = np.zeros((3, TEST_STEPS))  # ONLY LEG1
ACTUAL_JOINT_ANGLES = np.zeros((3, TEST_STEPS))  # ONLY LEG1

############## Sample Gains
# joint PD gains
kp = np.array([100, 100, 100])
kd = np.array([2, 2, 2])
# Cartesian PD gains
kpCartesian = np.diag([500] * 3)
kdCartesian = np.diag([20] * 3)

for j in range(TEST_STEPS):
    # initialize torque array to send to motors
    action = np.zeros(12)
    # get desired foot positions from CPG
    xs, zs = cpg.update()
    #print("xs",xs,'zs',zs)
    #  get current motor angles and velocities for joint PD, see GetMotorAngles(), GetMotorVelocities() in quadruped.py
    q = env.robot.GetMotorAngles()
    dq = env.robot.GetMotorVelocities()

    # loop through desired foot positions and calculate torques
    for i in range(4):
        # initialize torques for leg i
        tau = np.zeros(3)
        # get desired foot i pos (xi, yi, zi) in leg frame
        leg_xyz = np.array([xs[i], sideSign[i] * foot_y, zs[i]])
        # call inverse kinematics to get corresponding joint angles (see ComputeInverseKinematics() in quadruped.py)
        leg_q = env.robot.ComputeInverseKinematics(legID=i, xyz_coord=leg_xyz)
        # Add joint PD contribution to tau for leg i (Equation 4)
        ## NEED TO EXTRACT ANGLE FOR EACH LEG:
        tau += kp * (leg_q - q[3 * i:3 * i + 3]) + kd * (-dq[3 * i:3 * i + 3])

        # add Cartesian PD contribution
        if ADD_CARTESIAN_PD:
            # Get current Jacobian and foot position in leg frame (see ComputeJacobianAndPosition() in quadruped.py)
            J, foot_pos = env.robot.ComputeJacobianAndPosition(legID=i)
            # Get current foot velocity in leg frame (Equation 2)
            foot_vel = J @ dq[3 * i:3 * i + 3]
            # Calculate torque contribution from Cartesian PD (Equation 5) [Make sure you are using matrix multiplications]
            tau += J.T @ (kpCartesian @ (leg_xyz - foot_pos) + kdCartesian @ (leg_xyz - foot_vel))

        # Set tau for legi in action vector
        action[3 * i:3 * i + 3] = tau

    # send torques to robot and simulate TIME_STEP seconds
    env.step(action)

    # [] save any CPG or robot states
    ## GET CPG STATES FOR EACH LEG
    CPG_STATES[:, 0, j] = cpg.get_r()
    CPG_STATES[:, 1, j] = cpg.get_theta()
    CPG_STATES[:, 2, j] = cpg.get_dr()
    CPG_STATES[:, 3, j] = cpg.get_dtheta()

    DESIRED_FOOT_POSITIONS[:, j] = leg_xyz  # ONLY LEG1
    if ADD_CARTESIAN_PD:
        ACTUAL_FOOT_POSITIONS[:, j] = foot_pos  # ONLY LEG1
    DESIRED_JOINT_ANGLES[:, j] = leg_q[0: 3]  # ONLY LEG1
    ACTUAL_JOINT_ANGLES[:, j] = q[0:3]  # ONLY LEG1

#####################################################
# PLOTS
#####################################################
# Create some fake data.

# fig1: plot r, theta, dr, dtheta for all legs
# , (ax1, ax2, ax3, ax4, ax5, ax6, ax7, ax8, ax9, ax10, ax11, ax12, ax13, ax14, ax15, ax16)
fig1 = plt.figure("r, theta, dr, dtheta for all legs")
# r for 4 legs
plt.subplot(4, 4, 1)
plt.title("r for all legs")
plt.plot(t, CPG_STATES[0, 0, :], label='r leg 1')
plt.ylabel("leg 1")
plt.subplot(4, 4, 5)
plt.plot(t, CPG_STATES[1, 0, :], label='r leg 2')
plt.ylabel("leg 2")
plt.subplot(4, 4, 9)
plt.plot(t, CPG_STATES[2, 0, :], label='r leg 3')
plt.ylabel("leg 3")
plt.subplot(4, 4, 13)
plt.plot(t, CPG_STATES[3, 0, :], label='r leg 4')
plt.ylabel("leg 4")
plt.xlabel("time in s")
plt.legend(loc='upper right')

# theta for 4 legs
plt.subplot(4, 4, 2)
plt.title("dr for all legs")
plt.plot(t, CPG_STATES[0, 1, :], label='theta leg 1')
plt.subplot(4, 4, 6)
plt.plot(t, CPG_STATES[1, 1, :], label='theta leg 2')
plt.subplot(4, 4, 10)
plt.plot(t, CPG_STATES[2, 1, :], label='theta leg 3')
plt.subplot(4, 4, 14)
plt.plot(t, CPG_STATES[3, 1, :], label='theta leg 4')
plt.xlabel("time in s")
plt.legend(loc='upper right')

# dr for 4 legs
plt.subplot(4, 4, 3)
plt.title("r for all legs")
plt.plot(t, CPG_STATES[0, 2, :], label='dr leg 1')
plt.subplot(4, 4, 7)
plt.plot(t, CPG_STATES[1, 2, :], label='dr leg 2')
plt.subplot(4, 4, 11)
plt.plot(t, CPG_STATES[2, 2, :], label='dr leg 3')
plt.subplot(4, 4, 15)
plt.plot(t, CPG_STATES[3, 2, :], label='dr leg 4')
plt.xlabel("time in s")
plt.legend(loc='upper right')

# dtheta for 4 legs
plt.subplot(4, 4, 4)
plt.title("dr for all legs")
plt.plot(t, CPG_STATES[0, 3, :], label='dtheta leg 1')
plt.subplot(4, 4, 8)
plt.plot(t, CPG_STATES[1, 3, :], label='dtheta leg 2')
plt.subplot(4, 4, 12)
plt.plot(t, CPG_STATES[2, 3, :], label='dtheta leg 3')
plt.subplot(4, 4, 16)
plt.plot(t, CPG_STATES[3, 3, :], label='dtheta leg 4')
plt.xlabel("time in s")
plt.legend(loc='upper right')
plt.show()


# fig 2:
fig2 = plt.figure("plot for q1 leg")
plt.subplot(1, 2, 1)
plt.plot(t, DESIRED_FOOT_POSITIONS[0, :], label='desired foot pos x')
plt.plot(t, DESIRED_FOOT_POSITIONS[1, :], label='desired foot pos y')
plt.plot(t, DESIRED_FOOT_POSITIONS[2, :], label='desired foot pos z')
if ADD_CARTESIAN_PD:
    plt.plot(t, ACTUAL_FOOT_POSITIONS[0, :], label='actual foot pos x')
    plt.plot(t, ACTUAL_FOOT_POSITIONS[1, :], label='actual foot pos y')
    plt.plot(t, ACTUAL_FOOT_POSITIONS[2, :], label='actual foot pos z')
plt.legend(loc='center left')
plt.title("plots for leg1")
plt.subplot(1, 2, 2)
plt.plot(t, DESIRED_JOINT_ANGLES[0, :], label='desired q1')
plt.plot(t, DESIRED_JOINT_ANGLES[1, :], label='desired q2')
plt.plot(t, DESIRED_JOINT_ANGLES[2, :], label='desired q3')
plt.plot(t, ACTUAL_JOINT_ANGLES[0, :], label='actual q1')
plt.plot(t, ACTUAL_JOINT_ANGLES[1, :], label='actual q2')
plt.plot(t, ACTUAL_JOINT_ANGLES[2, :], label='actual q3')
plt.legend(loc='center left')
plt.title("plots for leg1")
plt.show()