# SPDX-FileCopyrightText: Copyright (c) 2022 Guillaume Bellegarda. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Copyright (c) 2022 EPFL, Guillaume Bellegarda

import os

import matplotlib.pyplot as plt
import numpy as np
from stable_baselines3 import PPO, SAC
# from stable_baselines3.common.cmd_util import make_vec_env
from stable_baselines3.common.env_util import make_vec_env  # fix for newer versions of stable-baselines3
# stable-baselines3
from stable_baselines3.common.monitor import load_results
from stable_baselines3.common.vec_env import VecNormalize

from env.quadruped_gym_env import QuadrupedGymEnv
from utils.file_utils import get_latest_model
# utils
from utils.utils import plot_results

# may be helpful depending on your system
# if platform =="darwin": # mac
#   import PyQt5
#   matplotlib.use("Qt5Agg")
# else: # linux
#   matplotlib.use('TkAgg')

LEARNING_ALG = "PPO"
interm_dir = "./logs/intermediate_models/"
# path to saved models, i.e. interm_dir + '121321105810'
log_dir = interm_dir + 'E_rew_0p5_coupling_vd_1_CPG_states_noise_v3'
# log_dir = interm_dir + 'joint_pd_v2'

# initialize env configs (render at test time)
# check ideal conditions, as well as robustness to UNSEEN noise during training
env_config = {}
env_config['render'] = True
env_config['record_video'] = True
env_config['add_noise'] = False
# env_config['competition_env'] = True

# get latest model and normalization stats, and plot 
stats_path = os.path.join(log_dir, "vec_normalize.pkl")
model_name = get_latest_model(log_dir)  # uncomment
monitor_results = load_results(log_dir)  # uncomment
# print(monitor_results)
plot_results([log_dir], 10e10, 'timesteps', LEARNING_ALG + ' ')  # uncomment
# plot_results_bad_csv(log_dir)  # comment
# tikzplotlib.save('C:/Users/ralf-/OneDrive/!Uni/Legged Robots/Project2/Plot files/training_curve_joint_pd2.tex', encoding="utf-8")
plt.show()

# reconstruct env 
env = lambda: QuadrupedGymEnv(**env_config)
env = make_vec_env(env, n_envs=1)
env = VecNormalize.load(stats_path, env)
env.training = False  # do not update stats at test time
env.norm_reward = False  # reward normalization is not needed at test time

# load model
if LEARNING_ALG == "PPO":
    model = PPO.load(model_name, env)
elif LEARNING_ALG == "SAC":
    model = SAC.load(model_name, env)
print("\nLoaded model", model_name, "\n")

obs = env.reset()
episode_reward = 0

# [] initialize arrays to save data from simulation
#
n_sim_steps = 1000
BASE_POSITION = np.zeros((3, n_sim_steps))
TIME = np.zeros((1, n_sim_steps))
CPG_r = np.zeros((4, n_sim_steps))
CPG_dr = np.zeros((4, n_sim_steps))
CPG_theta = np.zeros((4, n_sim_steps))
CPG_dtheta = np.zeros((4, n_sim_steps))

# Energy for computing the CoT
E = 0

for i in range(n_sim_steps):
    constant = 0
    last_time = env.envs[0].get_sim_time()

    action, _states = model.predict(obs, deterministic=True)  # sample at test time? ([TODO]: test)
    obs, rewards, dones, info = env.step(action)
    episode_reward += rewards
    if dones:
        print('episode_reward', episode_reward)
        print('Final base position', info[0]['base_pos'])
        episode_reward = 0
        constant = last_time

    # [] save data from current robot states for plots
    # To get base position, for example: env.envs[0].env.robot.GetBasePosition() 
    BASE_POSITION[:, i] = env.envs[0].env.robot.GetBasePosition()
    TIME[:, i] = constant + env.envs[0].get_sim_time()
    CPG_r[:, i] = env.envs[0].env._cpg.get_r()
    CPG_dr[:, i] = env.envs[0].env._cpg.get_dr()
    CPG_theta[:, i] = env.envs[0].env._cpg.get_theta()
    CPG_dtheta[:, i] = env.envs[0].env._cpg.get_dtheta()

    # Add energy
    for tau, vel in zip(env.envs[0].env._dt_motor_torques, env.envs[0].env._dt_motor_velocities):
        E += np.abs(np.dot(tau, vel)) * env.envs[0].env._time_step

# Calculate Cost of Transport
m = 12
g = 9.81
d = np.linalg.norm(env.envs[0].env.robot.GetBasePosition()[0:2])
CoT = E / (m * g * d)
print('CoT = ' + str(CoT) + ' with m = ' + str(m) + ', g = ' + str(g) + ' and d = ' + str(d))

# [] make plots:
fig1 = plt.figure("base position")
plt.plot(TIME[0, :], BASE_POSITION[0, :], label='x')
plt.plot(TIME[0, :], BASE_POSITION[1, :], label='y')
plt.plot(TIME[0, :], BASE_POSITION[2, :], label='z')
plt.legend()
# tikzplotlib.save('cpg_rl_pos.tex')
plt.show()

fig, axes = plt.subplots(2, 2)
axes[0, 0].plot(TIME[0, :], CPG_r[0, :], label='$r_1$')
axes[0, 0].plot(TIME[0, :], CPG_r[1, :], label='$r_2$')
axes[0, 0].plot(TIME[0, :], CPG_r[2, :], label='$r_3$')
axes[0, 0].plot(TIME[0, :], CPG_r[3, :], label='$r_4$')
axes[0, 1].plot(TIME[0, :], CPG_dr[0, :], label='$\\dot{r}_1$')
axes[0, 1].plot(TIME[0, :], CPG_dr[1, :], label='$\\dot{r}_2$')
axes[0, 1].plot(TIME[0, :], CPG_dr[2, :], label='$\\dot{r}_3$')
axes[0, 1].plot(TIME[0, :], CPG_dr[3, :], label='$\\dot{r}_4$')
axes[1, 0].plot(TIME[0, :], CPG_theta[0, :], label='$\\theta_1$')
axes[1, 0].plot(TIME[0, :], CPG_theta[1, :], label='$\\theta_2$')
axes[1, 0].plot(TIME[0, :], CPG_theta[2, :], label='$\\theta_3$')
axes[1, 0].plot(TIME[0, :], CPG_theta[3, :], label='$\\theta_4$')
axes[1, 1].plot(TIME[0, :], CPG_dtheta[0, :], label='$\\dot{\\theta}_1$')
axes[1, 1].plot(TIME[0, :], CPG_dtheta[1, :], label='$\\dot{\\theta}_2$')
axes[1, 1].plot(TIME[0, :], CPG_dtheta[2, :], label='$\\dot{\\theta}_3$')
axes[1, 1].plot(TIME[0, :], CPG_dtheta[3, :], label='$\\dot{\\theta}_4$')

plt.show()
